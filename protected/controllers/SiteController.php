<?php

namespace app\controllers;

use Yii,
    yii\filters\AccessControl,
    yii\web\Controller,
    app\models\LoginForm,
    app\models\User,
    app\models\TransferForm,
    app\models\Transfers;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Главная страница
     *
     * @return string
     */
    public function actionIndex()
    {
        $modelLogin = new LoginForm;
        $modelTransfer = new TransferForm;

        if (Yii::$app->request->isPost) {
            // POST авторизация
            if (Yii::$app->request->post('LoginForm') && $modelLogin->load(Yii::$app->request->post())) {
                $modelLogin->login();
            }
            // POST перевод
            if (Yii::$app->request->post('TransferForm') && $modelTransfer->load(Yii::$app->request->post())) {
                if (!Yii::$app->user->isGuest && $modelTransfer->validate()) {
                    $modelTransfer->run();
                    $modelTransfer->sum = '';
                }
            }
        }

        if (Yii::$app->user->isGuest) {
            $return = [
                'model' => $modelLogin,
            ];
        } else {
            $return = [
                'model' => $modelTransfer,
                'user' => Yii::$app->user->isGuest ? null : $modelLogin->getUserById(Yii::$app->user->id)
            ];
        }

        return $this->render('index', $return);
    }

    /**
     * Страница пользователей
     *
     * @return string
     */
    public function actionUsers()
    {
        $model = new User;
        return $this->render('users', [
            'model' => $model
        ]);
    }

    /**
     * Принять оплату счета
     *
     * @param $id
     */
    public function actionPay($id)
    {
        if ((int)$id) {
            Transfers::payInvoice($id);
        }
        $this->redirect('index');
    }

    /**
     * Отказаться от оплаты счета или удалить счет
     *
     * @param $id
     */
    public function actionCancel($id)
    {
        if ((int)$id) {
            Transfers::cancelInvoice($id);
        }
        $this->redirect('index');
    }

    /**
     * Выйти
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
