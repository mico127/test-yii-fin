<?php

namespace app\models;

use yii\db\ActiveRecord,
    yii\data\ActiveDataProvider;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where(['accessToken' => $token])->one();
    }

    /**
     * Finds user by username or create new
     *
     * @param string $username
     * @return Users $user
     */
    public static function findByUsername($username)
    {
        if (!$user = self::find()->where(['username' => $username])->one()) {
            $user = new self;
            $user->username = $username;
            $user->authKey = uniqid();
            $user->accessToken = 'to_' . uniqid();
            $user->save();
        }

        return $user;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Список всех пользователей с балансом
     *
     * @return ActiveDataProvider
     */
    public static function search()
    {
        $query = User::find();

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => 50
            ]
        ]);
    }
}
