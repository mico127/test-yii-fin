<?php

namespace app\models;

use Yii,
    yii\base\Model,
    yii\data\ActiveDataProvider;

class TransferForm extends Model
{
    public $username;
    public $type;
    public $sum;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'type', 'sum'], 'required'],
            [['type', 'sum'], 'number', 'min' => 1],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Кому',
            'type' => 'Тип перевода',
            'sum' => 'Сумма',
        ];
    }


    /**
     * Возвращает массив типов счетов
     *
     * @return array
     */
    public function getTypes()
    {
        return (new Transfers)->getTypes();
    }

    /**
     * Делает перевод или открывает счет
     */
    public function run()
    {
        switch ($this->type) {
            case Transfers::TYPE_DIRECT:
                Transfers::sendTransferDirect($this);
                break;
            case Transfers::TYPE_INVOICE:
                Transfers::sendTransferInvoice($this);
                break;
        }
    }

    /**
     * Таблица счетов
     *
     * @return ActiveDataProvider
     */
    public function searchInvoice()
    {
        $query = Transfers::find()->where('(user_id_from = :idfrom OR user_id_to = :idto) AND type = :type', ['idfrom' => Yii::$app->user->id, 'idto' => Yii::$app->user->id, 'type' => Transfers::TYPE_INVOICE])->orderBy('id DESC');

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => 20
            ]
        ]);
    }

    /**
     * Таблица прямых переводов
     *
     * @return ActiveDataProvider
     */
    public function searchDirect()
    {
        $query = Transfers::find()->where(['user_id_from' => Yii::$app->user->id, 'type' => Transfers::TYPE_DIRECT])->orderBy('id DESC');

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => 20
            ]
        ]);
    }
}
