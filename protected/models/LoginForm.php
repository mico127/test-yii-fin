<?php

namespace app\models;

use Yii,
    yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;

    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username'], 'required', 'message' => 'Введите имя пользователя.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Пользователь',
        ];
    }

    /**
     * Logs in a user using the provided username.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), 3600*24*30);
        }
        return false;
    }

    /**
     * Finds user by username or create new
     *
     * @return User|null
     */
    public function getUser()
    {
        if (!$this->_user) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * Finds user by id
     *
     * @return User|null
     */
    public function getUserById($id)
    {
        return User::findIdentity($id);
    }
}
