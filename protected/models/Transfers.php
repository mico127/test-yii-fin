<?php

namespace app\models;

use Yii,
    yii\db\ActiveRecord;

class Transfers extends ActiveRecord
{
    const TYPE_DIRECT = 1;
    const TYPE_INVOICE = 2;

    const STATUS_OK = 1;
    const STATUS_WAIT = 2;
    const STATUS_CANCEL = 3;
    const STATUS_PAY = 4;

    private $types = [
        1 => 'Прямой',
        2 => 'Счет'
    ];


    /**
     * Реляционно получаем пользователя перевода
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserTo()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id_to']);
    }

    /**
     * Реляционно получаем пользователя адресата
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id_from']);
    }

    /**
     * Вернуть типы операций
     *
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * Вернуть текст типа
     *
     * @return mixed|string
     */
    public function getTextType()
    {
        return isset($this->types[$this->type]) ? $this->types[$this->type] : '';
    }

    /**
     * Вернуть текст статуса
     *
     * @return string
     */
    public function getTextStatus()
    {
        switch ($this->status) {
            case self::STATUS_OK:
                return 'ОК';
            case self::STATUS_WAIT:
                return 'В ожидании';
            case self::STATUS_CANCEL:
                return 'Отменен/отказ';
            case self::STATUS_PAY:
                return 'Оплачен';
            default:
                return 'Неизвестная ошибка';
        }
    }

    private static function addToLog($id1, $id2, $type, $message, $status)
    {
        $transfer = new self;
        $transfer->user_id_from = $id1;
        $transfer->user_id_to = $id2;
        $transfer->type = $type;
        $transfer->message = $message;
        $transfer->status = $status;
        $transfer->date_create = date('Y-m-d H:i');
        $transfer->save();
    }

    /**
     * Делает прямой перевод денежных средств
     *
     * @param LoginForm $data
     */
    public static function sendTransferDirect($data)
    {
        $userFrom = User::findIdentity(Yii::$app->user->id);
        $userTo = User::findByUsername($data->username);
        if ($userFrom && $userTo && $userFrom->id != $userTo->id) {
            self::addToLog(
                $userFrom->id, $userTo->id, self::TYPE_DIRECT,
                'Сделан перевод средств в сумме «' . $data->sum . '» для пользователя «' . $userTo->username . '»',
                self::STATUS_OK
            );
            self::addToLog(
                $userTo->id, $userFrom->id, self::TYPE_DIRECT,
                'Вам сделан перевод на сумму «' . $data->sum . '» от пользователя «' . $userFrom->username . '»',
                self::STATUS_OK
            );
            $userFrom->balance -= $data->sum;
            $userFrom->save();
            $userTo->balance += $data->sum;
            $userTo->save();
        }
    }

    /**
     * Открывает счет для оплаты
     *
     * @param LoginForm $data
     */
    public static function sendTransferInvoice($data)
    {
        $userFrom = User::findIdentity(Yii::$app->user->id);
        $userTo = User::findByUsername($data->username);
        if ($userFrom && $userTo) {
            self::addToLog($userFrom->id, $userTo->id, self::TYPE_INVOICE, $data->sum, self::STATUS_WAIT);
        }
    }

    /**
     * Оплата счета
     *
     * @param int $id
     */
    public static function payInvoice($id)
    {
        if ($invoice = self::findOne($id)) {
            if (
                ($invoice->user_id_from == Yii::$app->user->id || $invoice->user_id_to == Yii::$app->user->id) &&
                ($invoice->type == self::TYPE_INVOICE)
            ) {
                $sum = (float)$invoice->message;
                $invoice->userFrom->balance += $sum;
                $invoice->userFrom->save();
                $invoice->userTo->balance -= $sum;
                $invoice->userTo->save();
                $invoice->status = self::STATUS_PAY;
                $invoice->save();
            }
        }
    }

    /**
     * Отмена счета
     *
     * @param int $id
     */
    public static function cancelInvoice($id)
    {
        if ($invoice = self::findOne($id)) {
            if (
                ($invoice->user_id_from == Yii::$app->user->id || $invoice->user_id_to == Yii::$app->user->id) &&
                ($invoice->type == self::TYPE_INVOICE)
            ) {
                $invoice->status = self::STATUS_CANCEL;
                $invoice->save();
            }
        }
    }

}
