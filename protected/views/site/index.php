<?php

/* @var $this yii\web\View */

use yii\helpers\Html,
    yii\bootstrap\ActiveForm,
    yii\grid\GridView;

$this->title = 'Test / Sergeev Dmitry / ragnhild.iceland@gmail.com';
?>
<div class="site-index">

    <div class="jumbotron">

        <?php if (Yii::$app->user->isGuest) { ?>

            <h1>Авторизация</h1>

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div>{input}</div>\n<div class=\"alert alert-info\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
            ]); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            <div class="alert alert-warning">Если не существует пользователь, то будет создан новый!</div>

            <div class="form-group">
                <div class="col-lg-offset-1 col-lg-11">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        <?php } else { ?>

            <h1>Здравствуйте, <?= $user->username ?></h1>

            <br/>
            <h3>
                <span class="label label-success">На вашем балансе: <?= $user->balance ?> марсианских долларов</span>
                <?= Html::a('выйти', ['/site/logout'], ['class' => 'btn btn-danger']) ?>
            </h3>

            <table class="table table-bordered">
                <tr>
                    <td valign="top">
                        <table class="table">
                            <tr>
                                <th class="text-info">СЧЕТА</th>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <?= GridView::widget([
                                        'dataProvider' => $model->searchInvoice(),
                                        'columns' => [
                                            [
                                                'attribute' => 'type',
                                                'header' => 'Тип',
                                                'content' => function($data){
                                                    return $data->getTextType();
                                                }
                                            ],
                                            [
                                                'attribute' => 'message',
                                                'header' => 'Сообщение',
                                                'content' => function($data) use($user){
                                                    $message = $data->user_id_from == $user->id ?
                                                        'Вы выставили счет на сумму «' . $data->message . '» для пользователя «' . $data->userTo->username . '»' :
                                                        'Вам выставлен счет на сумму «' . $data->message . '» от пользователя «' . $data->userFrom->username . '»';

                                                    return $message;
                                                }
                                            ],
                                            [
                                                'attribute' => 'status',
                                                'header' => 'Статус',
                                                'content' => function($data){
                                                    return $data->getTextStatus();
                                                }
                                            ],
                                            [
                                                'attribute' => 'date_create',
                                                'header' => 'Дата',
                                                'format' =>  ['date', 'd.M.Y HH:mm'],
                                            ],
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{PayOrDelete}',
                                                'buttons' => [
                                                    'PayOrDelete' => function ($url, $data) use($user) {
                                                        $pay = ($data->status == $data::STATUS_WAIT && $data->user_id_to == $user->id) ?
                                                            Html::a('Оплатить', ['/site/pay?id=' . $data->id]) . '&nbsp;|&nbsp;' :
                                                            '';
                                                        $trash = $data->status == $data::STATUS_WAIT ?
                                                            Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/site/cancel?id=' . $data->id]) :
                                                            '';
                                                        return $pay.$trash;
                                                    },
                                                ]
                                            ]
                                        ],
                                    ]) ?>
                                </td>
                            </tr>
                        </table>
                        <table class="table">
                            <tr>
                                <th class="text-info">ПРЯМЫЕ ПЕРЕВОДЫ</th>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <?= GridView::widget([
                                        'dataProvider' => $model->searchDirect(),
                                        'columns' => [
                                            [
                                                'attribute' => 'type',
                                                'header' => 'Тип',
                                                'content' => function($data){
                                                    return $data->getTextType();
                                                }
                                            ],
                                            'message:text:Сообщение',
                                            [
                                                'attribute' => 'status',
                                                'header' => 'Статус',
                                                'content' => function($data){
                                                    return $data->getTextStatus();
                                                }
                                            ],
                                            [
                                                'attribute' => 'date_create',
                                                'header' => 'Дата',
                                                'format' =>  ['date', 'd.M.Y HH:mm'],
                                            ]
                                        ],
                                    ]) ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="20%">
                        <h3>СДЕЛАТЬ ПЕРЕВОД ИЛИ ВЫСТАВИТЬ СЧЕТ</h3>
                        <?php $form = ActiveForm::begin(['id' => 'transfer-form']); ?>
                            <small>
                                <?= $form->field($model, 'username')->textInput() ?><br/>
                                <?= $form->field($model, 'type')->dropDownList($model->getTypes()) ?><br/>
                                <?= $form->field($model, 'sum')->textInput() ?>
                            </small>
                            <div>
                                <?= Html::submitButton('Выполнить', ['class' => 'btn btn-primary', 'name' => 'transfer-button']) ?>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </td>
                </tr>
            </table>
        <?php } ?>

    </div>
</div>
