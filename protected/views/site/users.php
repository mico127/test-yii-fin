<?php

/* @var $this yii\web\View */

use yii\grid\GridView;

$this->title = 'Test / Sergeev Dmitry / ragnhild.iceland@gmail.com';
?>
<div class="site-users">

    <div class="jumbotron">

        <h1>Список пользователей</h1>

        <?= GridView::widget([
            'dataProvider' => $model->search(),
            'columns' => [
                'id:text:ID',
                'username:text:Пользователь',
                'balance:text:Баланс',
            ],
        ]) ?>

    </div>
</div>
