<?php

use yii\db\Migration;

class m161215_180256_table_user extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(55)->notNull(),
            'balance' => $this->decimal(10,3)->defaultValue(0),
            'authKey' => $this->string(55),
            'accessToken' => $this->string(55),
        ]);
        $this->createIndex(
            'idx-username',
            'user',
            'username'
        );
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
