<?php

use yii\db\Migration;

class m161215_181138_table_transfers extends Migration
{
    public function up()
    {
        $this->createTable('transfers', [
            'id' => $this->primaryKey(),
            'user_id_from' => $this->integer()->notNull(),
            'user_id_to' => $this->integer()->notNull(),
            'type' => $this->integer(4),
            'message' => $this->text(),
            'status' => $this->integer(4),
            'date_create' => $this->datetime()
        ]);
        $this->createIndex(
            'idx-user_id_from',
            'transfers',
            'user_id_from'
        );
        $this->createIndex(
            'idx-user_id_to',
            'transfers',
            'user_id_to'
        );
    }

    public function down()
    {
        $this->dropTable('transfers');
    }
}
